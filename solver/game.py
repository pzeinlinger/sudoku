from typing import Iterator, Dict, List
from itertools import chain

from .cell import Cell


class Game(object):
    def __init__(self, initial_state_iterator: Iterator):
        self.cells: Dict[int, Cell] = {}
        self.rows: List[List[Cell]] = [[] for i in range(0, 9)]
        self.cols: List[List[Cell]] = [[] for i in range(0, 9)]
        self.blocks: List[List[Cell]] = [[] for i in range(0, 9)]
        for x in range(0, 9):
            for y in range(0, 9):
                cell = Cell((x, y))
                self.cells[hash(cell)] = cell
                self.rows[x].append(cell)
                self.cols[y].append(cell)
                self.blocks[x // 3 * 3 + y // 3].append(cell)
        self.peer_groups: Dict[int, List[List[Cell]]] = dict(
            (hash(cell), []) for cell in self.cells
        )
        for cell in self.cells.values():
            self.peer_groups[hash(cell)] = [
                [cell_i for cell_i in self.get_block_peers_of(cell) if cell_i != cell],
                [cell_i for cell_i in self.get_col_peers_of(cell) if cell_i != cell],
                [cell_i for cell_i in self.get_row_peers_of(cell) if cell_i != cell],
            ]
        numbers = 0
        for row in self.rows:
            row_iterator = iter(row)
            for cell in row_iterator:
                i = int(next(initial_state_iterator))
                if i > 0:
                    numbers += 1
                    if not self.reduce_options_to(cell, i):
                        raise ValueError("Invalid Game")
        if numbers < 17:
            raise ValueError(
                "Too few numbers referring to https://arxiv.org/abs/1201.0749"
            )

    def get_block_peers_of(self, cell: Cell) -> List[Cell]:
        x = cell.position[0]
        y = cell.position[1]
        return self.blocks[x // 3 * 3 + y // 3]

    def get_row_peers_of(self, cell: Cell) -> List[Cell]:
        x = cell.position[0]
        return self.rows[x]

    def get_col_peers_of(self, cell: Cell) -> List[Cell]:
        y = cell.position[1]
        return self.cols[y]

    def peers_of(self, cell) -> List[Cell]:
        return list(chain.from_iterable(self.peer_groups[hash(cell)]))

    @property
    def unresolved_cells(self):
        return set(filter(lambda cell: not cell, self.cells.values()))

    @property
    def has_unresolved_cells(self):
        return len(self.unresolved_cells) > 0

    @property
    def is_solved(self):
        return not self.has_unresolved_cells

    def reduce_options_to(self, cell: Cell, number: int):
        if all(self.remove_option(cell, i) for i in cell.options - set([number])):
            return self
        return None

    def remove_option(self, cell: Cell, number: int) -> bool:
        if number not in cell.options:
            return True
        if cell.is_set:
            return False
        cell.options.remove(number)
        if cell.is_set:
            if not all(
                self.remove_option(peer, cell.value) for peer in self.peers_of(cell)
            ):
                return False
        else:
            for group in self.peer_groups[hash(cell)]:
                possible_cells = [cell for cell in group if number in cell.options]
                if len(possible_cells) == 0:
                    return False
                elif len(possible_cells) == 1:
                    return self.reduce_options_to(possible_cells[0], number)
        return True

    def __str__(self):
        str_field = "\n"
        for x, row in enumerate(self.rows):
            if x > 0 and x % 3 == 0:
                str_field += "---------------------\n"
            for y, cell in enumerate(row):
                if y > 0 and y % 3 == 0:
                    str_field += "| "
                str_field += str(cell)
                str_field += " "
            str_field += "\n"
        return str_field

    def __repr__(self):
        return "".join([str(cell) for cell in sorted(self.cells.values())])
