import logging
import re
from solver.game import Game
import sys
from copy import deepcopy
from time import time

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
)
logger = logging.getLogger(__name__)


def backtrack(game: Game):
    if game is None:
        return None
    if game.is_solved:
        return game
    cell = min(game.unresolved_cells)
    for option in cell.options:
        new_game = deepcopy(game)
        new_cell = new_game.cells[hash(cell)]
        new_game = backtrack(new_game.reduce_options_to(new_cell, option))
        if new_game:
            return new_game


if __name__ == "__main__":
    numbers = []
    for line in sys.stdin:
        for match in re.finditer(r"(\d)", line):
            numbers.append(int(match[1]))
            if len(numbers) == 81:
                break

    start = time()
    game = Game(iter(numbers))
    solved = backtrack(game)
    duration = time() - start
    logger.info("Solved in {}".format(duration))
    logger.info(solved)
