import pytest

from solver.game import Game
from main import backtrack


@pytest.fixture
def game():
    return Game(
        iter(
            "700040180100009004080000003007180206010000357002034900600000030200500000059070001"
        )
    )


def test_backtrack(game: Game):
    solved_game = backtrack(game)
    assert solved_game.is_solved
    assert (
        repr(solved_game)
        == "795346182123859764486217593937185246814692357562734918678921435241563879359478621"
    )
