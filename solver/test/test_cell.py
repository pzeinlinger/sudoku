import pytest

from ..cell import Cell


@pytest.fixture
def cell():
    return Cell((1, 2))


def test_init(cell):
    assert len(cell.options) == 9
    assert cell.position == (1, 2)


def test_bool(cell):
    assert not cell


def test_hash(cell):
    assert hash(cell) == 12


def test_str(cell):
    assert str(cell) == "123456789"
