import pytest

from solver.game import Game
from solver.cell import Cell


@pytest.fixture
def game():
    return Game(
        iter(
            "475090208006403070090050004301009640609010087054386000000100402047932006160000090"
        )
    )


def test_init(game: Game):
    assert len(game.cells) == 81
    assert len(game.rows) == 9
    assert len(game.blocks) == 9
    assert any(cell.position == (1, 1) for cell in game.blocks[0])
    assert any(cell.position == (8, 8) for cell in game.blocks[8])
    assert any(cell.position == (2, 2) for cell in game.blocks[0])
    assert any(cell.position == (3, 2) for cell in game.blocks[3])
    assert any(cell.position == (2, 3) for cell in game.blocks[1])
    assert any(cell.position == (1, 1) for cell in game.cells.values())
    assert len(game.peer_groups.keys()) == 81
    assert all(
        len(peers) == 8
        for _, peer_group in game.peer_groups.items()
        for peers in peer_group
    )


def test_print(game: Game):
    print(game)


def test_unresolved_cells(game: Game):
    assert len(game.unresolved_cells) == 0
    assert not game.has_unresolved_cells


def test_peers_of(game: Game):
    peers = (
        10,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        1,
        21,
        31,
        41,
        51,
        61,
        71,
        81,
        0,
        2,
        20,
        22,
    )
    for peer in game.peers_of(Cell((1, 1))):
        assert hash(peer) in peers


def test_validate():
    with pytest.raises(ValueError):
        Game(
            iter(
                "1100000000000000000000000000000000000000000000000000000000000000000000000000000000"
            )
        )


def test_too_few_numbers():
    with pytest.raises(ValueError):
        Game(
            iter(
                "1234567890000000000000000000000000000000000000000000000000000000000000000000000000000"
            )
        )
