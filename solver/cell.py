from typing import Tuple


class Cell(object):
    def __init__(self, position: Tuple[int, int]):
        self.options = set(range(1, 10))
        self.position = position

    @property
    def is_set(self):
        return bool(self)

    @property
    def value(self):
        return next(iter(self.options)) if self.is_set else 0

    def __str__(self):
        return "".join([str(i) for i in self.options])

    def __hash__(self):
        return self.position[0] * 10 + self.position[1]

    def __bool__(self):
        return len(self.options) == 1

    def __eq__(self, value):
        return hash(self) == hash(value)

    def __lt__(self, value):
        return len(self.options) < len(value.options)
