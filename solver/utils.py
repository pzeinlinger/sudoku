def flatten_list(input):
    return [y for x in input for y in x]


def cross_product(A, B):
    return [a + b for a in A for b in B]
